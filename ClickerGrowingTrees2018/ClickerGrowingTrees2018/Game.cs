using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ClickerGrowingTrees2018
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        /* ��������� XNA */
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;



        /* ���������������� ���������� */
        int
            nStageGame,                 // ����� ���� (0 - ����, 1 - ����, 3 - ����� ����)
            nStageMenu,                 // ����� ���� (0 - ���� �����, 1 - �������)
            nStageLvl,                  // ����� ������� (0 - 1 ��., 1 - 2 ��.)
            nStageFinal;                // �������� ������ (0 - ������, 1 - ���������)         
        int[]
            aClickEnd;                  // ����������� ���-�� ������� ��� ������ �� 1 ��� 2 ��.
        float[]
            aTimerLvl;                  // ������ ��� �������
        string
            sTimerLvl;                  // ��������� ���������� ��� ������� (��� ���������)
        Random
            rndRandomValue;             // ����� ��� ������������
        MouseState
            msMouse;                    // ����� ��� ������������ �������� ����
        KeyboardState
            ksKeys;                     // ����� ��� ������������ �������� ����������
        Song[]
            songMusic;                  // ������ ��� ����
        bool[]
            isFlagMusic;                // ���� ��� ���/���� ������ 
        Buttons[]
            aButtons;                   // ������ ������� ������ ��� ����
        InputText
            _InputText;                 // ����� ��������� ����� ������
        Player
            cPlayer;                    // ����� �������� �������� �������
        List<Cloud>
            listCloudsLvl1,             // ����� ������� ��� ������� ������
            listCloudsLvl2;             // ����� ������� ��� ������� ������
        List<Bonus>
            listBonuses;                // ������ �������
        List<Trap>
            listTraps;                  // ������ �������
        List<Salyut>
            listAnimationSaluy;         // ������ ������� �������
        List<Rain>
            listRain;                   // ������ ������� ��� �����

        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }


        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // �������� �������� ��� ������ ������
            aButtons = new Buttons[6];
            aButtons[0] = new Buttons(Content.Load<Texture2D>("btnPlay"), new Vector2(900/2, 350));
            aButtons[1] = new Buttons(Content.Load<Texture2D>("btnRaiting"), new Vector2(900/2 - 900/ 4, 450));
            aButtons[2] = new Buttons(Content.Load<Texture2D>("btnExit"), new Vector2(900 / 2 + 900 / 4, 450));
            aButtons[3] = new Buttons(Content.Load<Texture2D>("btnCancel"), new Vector2(750, 70));
            aButtons[4] = new Buttons(Content.Load<Texture2D>("btnExit"), new Vector2(900 / 2 + 25, 500));
            aButtons[5] = new Buttons(Content.Load<Texture2D>("btnExit"), new Vector2(900 / 2 + 25, 500));
            // �������� �������� ��� ����� ����� ������
            _InputText = new InputText(new Vector2(230, 125), Content.Load<SpriteFont>("sfInputText"));
            // �������� �������� ��� �������� �������� �������
            cPlayer = new Player(Content.Load<Texture2D>("objMainTree"), new Vector2(450, 550));
            // �������� �������� ��� �������
            rndRandomValue = new Random();
            listCloudsLvl1 = new List<Cloud>();
            for (int i = 0; i < 10; i++)
            {
                listCloudsLvl1.Add(new Cloud(Content.Load<Texture2D>("objCloud"), new Vector2(rndRandomValue.Next(0, 900), rndRandomValue.Next(0, 430)), rndRandomValue.Next(1, 10) * 0.1f));
            }
            listCloudsLvl2 = new List<Cloud>();
            for (int i = 0; i < 5; i++)
            {
                listCloudsLvl2.Add(new Cloud(Content.Load<Texture2D>("objCloud"), new Vector2(rndRandomValue.Next(0, 900), rndRandomValue.Next(0, 430)), rndRandomValue.Next(1, 10) * 0.1f));
            }
            // �������� �������� ��� �������
            listBonuses = new List<Bonus>();
            for (int i = 0; i < 3; i++)
            {
                listBonuses.Add(new Bonus(Content.Load<Texture2D>("objBonus"), new Vector2(rndRandomValue.Next(200, 700), rndRandomValue.Next(-700, 0)), Content.Load<SoundEffect>("SoundEffect")));
            }
            // �������� �������� ��� �������
            listTraps = new List<Trap>();
            for (int i = 0; i < 15; i++)
            {
                listTraps.Add(new Trap(Content.Load<Texture2D>("objTrap"), new Vector2(rndRandomValue.Next(200, 700), rndRandomValue.Next(-1500, 0)), Content.Load<SoundEffect>("SoundEffect")));
            }
            // �������� �������� �����
            songMusic = new Song[3];
            songMusic[0] = Content.Load<Song>("musicLvl1");
            songMusic[1] = Content.Load<Song>("musicLvl2");
            isFlagMusic = new bool[2];
            isFlagMusic[0] = false;
            isFlagMusic[1] = false;
            // �������� �������� ��� ������
            listAnimationSaluy = new List<Salyut>();
            listAnimationSaluy.Add(new Salyut(Content.Load<Texture2D>("objSalyt"), new Vector2(250, 200), Color.Yellow));
            listAnimationSaluy.Add(new Salyut(Content.Load<Texture2D>("objSalyt"), new Vector2(450, 200), Color.Red));
            listAnimationSaluy.Add(new Salyut(Content.Load<Texture2D>("objSalyt"), new Vector2(650, 200), Color.Yellow));
            listAnimationSaluy.Add(new Salyut(Content.Load<Texture2D>("objSalyt"), new Vector2(350, 300), Color.Orange));
            listAnimationSaluy.Add(new Salyut(Content.Load<Texture2D>("objSalyt"), new Vector2(550, 300), Color.Orange));
            // �������� �������� ��� �����
            listRain = new List<Rain>();
            for (int i = 0; i < 100; i++)
            {
                listRain.Add(new Rain(Content.Load<Texture2D>("objRain"), new Vector2(rndRandomValue.Next(0, 900), rndRandomValue.Next(-100, 700))));
            }
            IsMouseVisible = true;
            graphics.PreferredBackBufferWidth = 900;
            graphics.PreferredBackBufferHeight = 600;
            graphics.ApplyChanges();
            nStageGame = 0;
            nStageMenu = 0;
            nStageLvl = 0;
            nStageFinal = -1;
            aTimerLvl = new float[2];
            aTimerLvl[0] = 20;
            aTimerLvl[1] = 30;
            sTimerLvl = "";
            aClickEnd = new int[2];
            aClickEnd[0] = 30;
            aClickEnd[1] = 80;
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                this.Exit();
            }
            msMouse = Mouse.GetState();
            Rectangle rRectMouse = new Rectangle(msMouse.X, msMouse.Y, 1, 1);
            ksKeys = Keyboard.GetState();
            switch (nStageGame)
            {
                case 0:
                    // ���� �������� ����
                    switch (nStageMenu)
                    {
                        case 0:
                            // ���� �������� ����
                            // ���������� ������
                            if (isFlagMusic[0] == false)
                            {
                                MediaPlayer.Play(songMusic[0]);
                                MediaPlayer.IsRepeating = true;
                                MediaPlayer.Volume = 0.3f;
                                isFlagMusic[0] = true;
                            }
                            // ��������� ������
                            // ������ "����� ����"
                            aButtons[0].Update(msMouse);
                            if ((aButtons[0].isClicked == true) || (ksKeys.IsKeyDown(Keys.Enter)))
                            {
                                if ((_InputText.sFullText.Length <= 10) && (_InputText.sFullText.Length > 0))
                                {
                                    cPlayer.sName = _InputText.sFullText;
                                    nStageGame = 1;
                                    nStageLvl = 0;
                                }
                            }
                            // ������ "�������"
                            aButtons[1].Update(msMouse);
                            if (aButtons[1].isClicked == true)
                            {
                                nStageMenu = 1;
                            }
                            // ������ "�����"
                            aButtons[2].Update(msMouse);
                            if (aButtons[2].isClicked == true)
                            {
                                this.Exit();
                            }
                            // ��������� �������� ����� ����� */
                            _InputText.Update(gameTime);
                            break;
                        case 1:
                            // ���� ��������
                            aButtons[3].Update(msMouse);
                            if (aButtons[3].isClicked == true)
                            {
                                nStageMenu = 0;
                            }
                            break;
                    }
                    break;
                case 1:
                    // ���� �������� ��������
                    // ���������� ������
                    if (isFlagMusic[1] == false)
                    {
                        MediaPlayer.Play(songMusic[1]);
                        MediaPlayer.IsRepeating = true;
                        MediaPlayer.Volume = 0.3f;
                        isFlagMusic[1] = true;
                    }
                    cPlayer.Update(gameTime);
                    switch (nStageLvl)
                    {
                        case 0:
                            // ���� ������� ������ ����
                            // ��������� �������
                            for (int i = 0; i < listCloudsLvl1.Count; i++)
                            {
                                listCloudsLvl1[i].Update(gameTime);
                            }
                            // ������ �������
                            aTimerLvl[0] -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                            if ((aTimerLvl[0] >= 0) && (aTimerLvl[0] < 10))
                            {
                                sTimerLvl = "0" + ((int)aTimerLvl[0]).ToString();
                            }
                            else
                            {
                                sTimerLvl = ((int)aTimerLvl[0]).ToString();
                            }
                            // ������ ������ � ������� ������
                            if (cPlayer.nClick >= aClickEnd[0])
                            {
                                // ������� �� 2 ������� ����
                                nStageLvl = 1;
                            }
                            // ������� ���������
                            if ((aTimerLvl[0] <= 0) && (cPlayer.nClick < aClickEnd[0])) 
                            {
                                // ������� �� ���� ��������� ���� � ����� ���������
                                nStageGame = 2;
                                nStageFinal = 1;
                            }
                            break;
                        case 1:
                            // ���� ������� ������ ����
                            // ��������� �������
                            for (int i = 0; i < listCloudsLvl2.Count; i++)
                            {
                                listCloudsLvl2[i].Update(gameTime);
                            }
                            // ������ �������
                            aTimerLvl[1] -= (float)gameTime.ElapsedGameTime.TotalSeconds;
                            if ((aTimerLvl[1] >= 0) && (aTimerLvl[1] < 10))
                            {
                                sTimerLvl = "0" + ((int)aTimerLvl[1]).ToString();
                            }
                            else
                            {
                                sTimerLvl = ((int)aTimerLvl[1]).ToString();
                            }
                            // ������ ������ � ������� ������
                            if (cPlayer.nClick >= 300)
                            {
                                // ������� �� ���� ��������� ���� � ����� ������
                                nStageGame = 2;
                                nStageFinal = 0;
                            }
                            // ������� ��������� ����
                            if (aTimerLvl[1] <= 0)
                            {
                                if (cPlayer.nClick < aClickEnd[1])
                                {
                                    // ������� �� ���� ��������� ���� � ����� ���������
                                    nStageGame = 2;
                                    nStageFinal = 1;
                                }
                                else
                                {
                                    // ������� �� ���� ��������� ���� � ����� ������
                                    nStageGame = 2;
                                    nStageFinal = 0;
                                }
                            }
                            // ����������� �������
                            for (int i = 0; i < listBonuses.Count; i++)
                            {
                                listBonuses[i].Update(gameTime);
                                if ((!listBonuses[i].isActive) && (!listBonuses[i].isUse))
                                {
                                    cPlayer.nClick += 10;
                                    listBonuses[i].isUse = true;
                                }
                            }
                            // ����������� �������
                            for (int i = 0; i < listTraps.Count; i++)
                            {
                                listTraps[i].Update(gameTime);
                                if (cPlayer.rRectInt.Intersects(listTraps[i].rRectInt))
                                {
                                    nStageGame = 2;
                                    nStageFinal = 1;
                                }
                            }
                            break;
                    }
                    break;
                case 2:
                    // ���� ��������� ����
                    switch (nStageFinal)
                    {
                        case 0:
                            aButtons[4].Update(msMouse);
                            if (aButtons[4].isClicked == true)
                            {
                                SaveResult(cPlayer.sName, cPlayer.nClick);
                                RestartGame();
                            }
                            for (int i = 0; i < listAnimationSaluy.Count; i++)
                            {
                                listAnimationSaluy[i].Update(gameTime);
                            }
                            break;
                        case 1:
                            aButtons[5].Update(msMouse);
                            if (aButtons[5].isClicked == true)
                            {
                                RestartGame();
                            }
                            for (int i = 0; i < listRain.Count; i++)
                            {
                                listRain[i].Update(gameTime);
                            }
                            break;
                    }
                    break;
            }



            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null);
            switch (nStageGame)
            {
                case 0:
                    // ���� �������� ����
                    switch (nStageMenu)
                    {
                        case 0:
                            // ���� �������� ����
                            spriteBatch.Draw(Content.Load<Texture2D>("BgMenu"), new Rectangle(0, 0, 900, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            spriteBatch.Draw(Content.Load<Texture2D>("elementInputText"), new Rectangle(50, 200, 800, 21), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            spriteBatch.Draw(Content.Load<Texture2D>("elementTitle"), new Rectangle(100, 10, 704, 112), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.89f);
                            aButtons[0].Draw(spriteBatch);
                            aButtons[1].Draw(spriteBatch);
                            aButtons[2].Draw(spriteBatch);
                            _InputText.Draw(spriteBatch);
                            break;
                        case 1:
                            // ���� ��������
                            spriteBatch.Draw(Content.Load<Texture2D>("BgRaiting"), new Rectangle(0, 0, 900, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            LoadResult();
                            aButtons[3].Draw(spriteBatch);
                            break;
                    }
                    break;
                case 1:
                    // ���� �������� ��������
                    cPlayer.Draw(spriteBatch);
                    /*
                     * ��������� ������ �������������� ��� �������������� (������� ������)
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "1", new Vector2(cPlayer.rRectInt.X, cPlayer.rRectInt.Y), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "2", new Vector2(cPlayer.rRectInt.X + cPlayer.rRectInt.Width, cPlayer.rRectInt.Y), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "3", new Vector2(cPlayer.rRectInt.X + cPlayer.rRectInt.Width, cPlayer.rRectInt.Y + cPlayer.rRectInt.Height), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "4", new Vector2(cPlayer.rRectInt.X, cPlayer.rRectInt.Y + cPlayer.rRectInt.Height), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                    */
                    // ���������� UI: �����������
                    spriteBatch.Draw(Content.Load<Texture2D>("Ui"), new Rectangle(10, 10, 482, 160), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.15f);
                    // ���������� UI: ����� - ���
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), cPlayer.sName, new Vector2(20, 15), Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.14f);
                    // ���������� UI: ����� - ���-�� ������
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), cPlayer.nClick.ToString(), new Vector2(110, 105), Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.14f);



                    switch (nStageLvl)
                    {
                        case 0:
                            // ���� ������� ������ ����
                            spriteBatch.Draw(Content.Load<Texture2D>("BgLvl1"), new Rectangle(0, 0, 900, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            // ��������� �������
                            for (int i = 0; i < listCloudsLvl1.Count; i++)
                            {
                                listCloudsLvl1[i].Draw(spriteBatch);
                            }
                            // ���������� UI: ����� - ����������
                            spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "��� �������� �� 2 ��. ���������� " + aClickEnd[0] + " ������", new Vector2(550, 570), Color.DarkOrange, 0f, new Vector2(0, 0), 0.3f, SpriteEffects.None, 0.14f);
                            break;
                        case 1:
                            // ���� ������� ������ ����
                            spriteBatch.Draw(Content.Load<Texture2D>("BgLvl2"), new Rectangle(0, 0, 900, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "�������� ������� " + aClickEnd[1] + " ������.", new Vector2(550, 570), Color.DarkOrange, 0f, new Vector2(0, 0), 0.3f, SpriteEffects.None, 0.14f);
                            // ��������� �������
                            for (int i = 0; i < listCloudsLvl2.Count; i++)
                            {
                                listCloudsLvl2[i].Draw(spriteBatch);
                            }
                            // ��������� �������
                            for (int i = 0; i < listBonuses.Count; i++)
                            {
                                listBonuses[i].Draw(spriteBatch);
                            }
                            // ��������� �������
                            for (int i = 0; i < listTraps.Count; i++)
                            {
                                listTraps[i].Draw(spriteBatch);
                                /*
                                 * ��������� ������ �������������� ��� ��������������  (�������)
                                spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "1", new Vector2(listTraps[i].rRectInt.X, listTraps[i].rRectInt.Y), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                                spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "2", new Vector2(listTraps[i].rRectInt.X + listTraps[i].rRectInt.Width, listTraps[i].rRectInt.Y), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                                spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "3", new Vector2(listTraps[i].rRectInt.X + listTraps[i].rRectInt.Width, listTraps[i].rRectInt.Y + listTraps[i].rRectInt.Height), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                                spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), "4", new Vector2(listTraps[i].rRectInt.X, listTraps[i].rRectInt.Y + listTraps[i].rRectInt.Height), Color.Black, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0.14f);
                                */
                            }
                            break;
                    }
                    // ���������� UI: ����� - ������
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), sTimerLvl, new Vector2(415, 17), Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0.14f);
                    break;
                case 2:
                    // ���� ��������� ����

                    switch (nStageFinal)
                    {
                        case 0:
                            spriteBatch.Draw(Content.Load<Texture2D>("BgWin"), new Rectangle(0, 0, 900, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            aButtons[4].Draw(spriteBatch);
                            spriteBatch.DrawString(Content.Load<SpriteFont>("sfUI"), cPlayer.nClick.ToString(), new Vector2(370, 120), Color.White, 0f, new Vector2(0, 0), 3f, SpriteEffects.None, 0.14f);
                            for (int i = 0; i < listAnimationSaluy.Count; i++)
                            {
                                listAnimationSaluy[i].Draw(spriteBatch);
                            }
                            break;
                        case 1:
                            spriteBatch.Draw(Content.Load<Texture2D>("BgDefeat"), new Rectangle(0, 0, 900, 600), null, Color.White, 0f, new Vector2(0, 0), SpriteEffects.None, 0.9f);
                            aButtons[5].Draw(spriteBatch);
                            for (int i = 0; i < listRain.Count; i++)
                            {
                                listRain[i].Draw(spriteBatch);
                            }
                            break;
                    }

                    break;
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }
        void RestartGame()
        {
            LoadContent();
        }
        void SaveResult(string _PlayerName, int _PlayerXP)
        {
            string _PlayerDate = DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string _CurrentDirectory = Environment.CurrentDirectory;
            FileStream _FileStream;
            XmlDocument _XmlDocument;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Create);
                if (_XmlDocument["Game"].ChildNodes.Count <= 10)
                {
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                else
                {
                    _XmlDocument["Game"].RemoveAll();
                    XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                    XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                    _XmlAttribute1.Value = _PlayerName;
                    _XmlNode.Attributes.Append(_XmlAttribute1);
                    XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                    _XmlAttribute2.Value = _PlayerXP.ToString();
                    _XmlNode.Attributes.Append(_XmlAttribute2);
                    XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                    _XmlAttribute3.Value = _PlayerDate;
                    _XmlNode.Attributes.Append(_XmlAttribute3);
                    _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                }
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
            else
            {
                _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.CreateNew);
                _XmlDocument = new XmlDocument();
                _XmlDocument.LoadXml("<Game></Game>");
                XmlNode _XmlNode = _XmlDocument.CreateElement("Save");
                XmlAttribute _XmlAttribute1 = _XmlDocument.CreateAttribute("Name");
                _XmlAttribute1.Value = _PlayerName;
                _XmlNode.Attributes.Append(_XmlAttribute1);
                XmlAttribute _XmlAttribute2 = _XmlDocument.CreateAttribute("XP");
                _XmlAttribute2.Value = _PlayerXP.ToString();
                _XmlNode.Attributes.Append(_XmlAttribute2);
                XmlAttribute _XmlAttribute3 = _XmlDocument.CreateAttribute("Date");
                _XmlAttribute3.Value = _PlayerDate;
                _XmlNode.Attributes.Append(_XmlAttribute3);
                _XmlDocument.DocumentElement.AppendChild(_XmlNode);
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
        }

        void LoadResult()
        {
            string _CurrentDirectory = Environment.CurrentDirectory;
            if (File.Exists(_CurrentDirectory + @"\Save.xml"))
            {
                FileStream _FileStream = new FileStream(_CurrentDirectory + @"\Save.xml", FileMode.Open);
                XmlDocument _XmlDocument = new XmlDocument();
                _XmlDocument.Load(_FileStream);
                _FileStream.Close();
                List<string[]> listRaiting = new List<string[]>();
                for (int i = 0; i < _XmlDocument["Game"].ChildNodes.Count; i++)
                {
                    string[] aRaitingRow = new string[4];
                    aRaitingRow[0] = _XmlDocument["Game"].ChildNodes[i].Attributes["Name"].InnerText;
                    aRaitingRow[1] = _XmlDocument["Game"].ChildNodes[i].Attributes["XP"].InnerText;
                    aRaitingRow[2] = _XmlDocument["Game"].ChildNodes[i].Attributes["Date"].InnerText;
                    if (i == _XmlDocument["Game"].ChildNodes.Count - 1) aRaitingRow[3] = "last";
                    else aRaitingRow[3] = "";
                    listRaiting.Add(aRaitingRow);
                }
                YearComparer yc = new YearComparer();
                listRaiting.Sort(yc);
                string _PlayerName = "";
                string _PlayerXP = "";
                string _PlayerDate = "";
                int _interval = 100;
                Color colorText;
                spriteBatch.DrawString(Content.Load<SpriteFont>("sfRaiting"), "���", new Vector2(100, 130), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("sfRaiting"), "��������� ����", new Vector2(350, 130), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("sfRaiting"), "����", new Vector2(750, 130), Color.White);
                for (int i = 0; i < listRaiting.Count; i++)
                {
                    _PlayerName = listRaiting[i][0];
                    _PlayerXP = listRaiting[i][1];
                    _PlayerDate = listRaiting[i][2];
                    colorText = Color.Black;
                    if (listRaiting[i][3] == "last") colorText = Color.Green;
                    else colorText = Color.White;
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfRaiting"), _PlayerName, new Vector2(100, 70 + _interval), colorText);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfRaiting"), _PlayerXP, new Vector2(450, 70 + _interval), colorText);
                    spriteBatch.DrawString(Content.Load<SpriteFont>("sfRaiting"), _PlayerDate, new Vector2(700, 70 + _interval), colorText);
                    _interval += 30;
                }
            }
        }

        class YearComparer : IComparer<string[]>
        {
            public int Compare(string[] o1, string[] o2)
            {
                int a = Convert.ToInt32(o1[1]);
                int b = Convert.ToInt32(o2[1]);

                if (a < b)
                {
                    return 1;
                }
                else if (a > b)
                {
                    return -1;
                }

                return 0;
            }
        }
    }
}
