﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace ClickerGrowingTrees2018
{
    class Objects
    {
        protected Texture2D
            t2dTexture;         // Текстура
        protected Rectangle
            rRect;              // Прямоугольник для отрисовки
        protected Vector2
            v2Position,         // Вектор позиции
            v2PositionOriginal; // Вектор оригинальной позиции
        protected float
            fSize;              // Множитель объекта
        public Color
            colorObject;        // Цвет объекта
        public bool
           isActive;            // Флаг активации объекта 
        public Objects(Texture2D new_t2dTexture, Vector2 new_v2Position)
        {
            t2dTexture = new_t2dTexture;
            v2Position = new_v2Position;
            colorObject = new Color(255, 255, 255, 255);
            fSize = 1f;
            isActive = true;
        }

        public void Update(GameTime gameTime)
        {
            if (!isActive)
            {
                return;
            }
            rRect = new Rectangle((int)v2Position.X, (int)v2Position.Y, t2dTexture.Width, t2dTexture.Height);
            v2PositionOriginal = new Vector2(rRect.Width / 2, rRect.Height / 2);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!isActive)
            {
                return;
            }
            spriteBatch.Draw(t2dTexture, v2Position, new Rectangle(0, 0, t2dTexture.Width, t2dTexture.Height), colorObject, 0f, v2PositionOriginal, fSize, SpriteEffects.None, 0.7f);
        }


    }

    class Cloud : Objects
    {
        float
            fTimer;         // Таймер для движения
        public Cloud(Texture2D new_t2dTexture, Vector2 new_v2Position, float new_fSize) : base(new_t2dTexture, new_v2Position)
        {
            fTimer = 0;
            fSize = new_fSize;
        }

        public new void Update(GameTime gameTime)
        {
            fTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (fTimer > 10)
            {
                v2Position.X += 2;
                fTimer = 0;
            }
            if (v2Position.X > 900 + 50 + t2dTexture.Width/2)
            {
                v2Position.X = 0 - 50 - t2dTexture.Width / 2;
            }

        }
    }

    class Bonus : Objects
    {
        Rectangle
            rRectMouse;         // Прямоугольник для взаимодействия с мышей
        MouseState
            msMouse;            // Класс для отслеживания действия мыши
        SoundEffect
           songMusic;          // Звуковой эффект
        float[]
            aTimer;             // Таймеры для движений
        bool
            isRotete;           // Флаг для направления движения
        float
            nStartPostionX;     // Начальная точка движения по оси 0X
        public Rectangle
            rRectInt;           // Прямоугольник для взаимодействия с другими объектами
        public bool
            isUse;              // Флаг использования объекта

        public Bonus(Texture2D new_t2dTexture, Vector2 new_v2Position, SoundEffect new_songMusic) : base(new_t2dTexture, new_v2Position)
        {
            aTimer = new float[2];
            aTimer[0] = 0;
            aTimer[1] = 1;
            isRotete = false;
            nStartPostionX = new_v2Position.X;
            isUse = false;
            songMusic = new_songMusic;
        }

        public new void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            msMouse = Mouse.GetState();
            rRectInt = new Rectangle(rRect.X - 25, rRect.Y - 25, rRect.Width - 50, rRect.Height - 50);
            rRectMouse = new Rectangle(msMouse.X, msMouse.Y, 1, 1);

            // Движение по 0Y
            aTimer[0] += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (aTimer[0] > 10)
            {
                v2Position.Y += 1;
                aTimer[0] = 0;
            }
            // Движение по 0X
            aTimer[1] += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (v2Position.X > nStartPostionX + 200)
            {
                isRotete = false;
            }
            if (v2Position.X < nStartPostionX - 200)
            {
                isRotete = true;
            }
            if (aTimer[1] > 12)
            {
                if (isRotete)
                {
                    v2Position.X += 1;
                }
                else
                {
                    v2Position.X -= 1;
                }
                aTimer[1] = 0;
            }
            if (msMouse.LeftButton == ButtonState.Pressed)
            {
                if (rRectInt.Intersects(rRectMouse))
                {
                    isActive = false;
                    Deleted();
                }
            }
        }
        private void Deleted()
        {
            songMusic.Play();
            v2Position = new Vector2(-500, -500);
            rRect = new Rectangle(-500, -500, 0, 0);
        }
    }

    class Trap : Objects
    {
        Rectangle
            rRectMouse;         // Прямоугольник для взаимодействия с мышей
        MouseState
            msMouse;            // Класс для отслеживания действия мыши
        SoundEffect
           songMusic;          // Звуковой эффект
        float[]
            aTimer;             // Таймеры для движений
        bool
            isRotete;           // Флаг для направления движения
        float
            nStartPostionX;     // Начальная точка движения по оси 0X
        public Rectangle
            rRectInt;           // Прямоугольник для взаимодействия с другими объектами


        public Trap(Texture2D new_t2dTexture, Vector2 new_v2Position, SoundEffect new_songMusic) : base(new_t2dTexture, new_v2Position)
        {
            aTimer = new float[2];
            aTimer[0] = 0;
            aTimer[1] = 1;
            isRotete = false;
            nStartPostionX = new_v2Position.X;
            songMusic = new_songMusic;
        }

        public new void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            msMouse = Mouse.GetState();
            rRectInt = new Rectangle(rRect.X - 25, rRect.Y - 25, rRect.Width - 50, rRect.Height - 50);
            rRectMouse = new Rectangle(msMouse.X, msMouse.Y, 1, 1);
            // Движение по 0Y
            aTimer[0] += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (aTimer[0] > 10)
            {
                v2Position.Y += 1;
                aTimer[0] = 0;
            }
            // Движение по 0X
            aTimer[1] += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (v2Position.X > nStartPostionX + 200)
            {
                isRotete = false;
            }
            if (v2Position.X < nStartPostionX - 200)
            {
                isRotete = true;
            }
            if (aTimer[1] > 12)
            {
                if (isRotete)
                {
                    v2Position.X += 1;
                }
                else
                {
                    v2Position.X -= 1;
                }
                aTimer[1] = 0;
            }
            if (msMouse.LeftButton == ButtonState.Pressed)
            {
                if (rRectInt.Intersects(rRectMouse))
                {
                    isActive = false;
                    Deleted();
                }
            }
        }
        private void Deleted()
        {
            songMusic.Play();
            v2Position = new Vector2(-500, -500);
            rRect = new Rectangle(-500, -500, 0, 0);
        }
    }

    class Salyut : Objects
    {
        int 
            FrameHeight, 
            FrameWidth, 
            FrameCurrent;
        float 
            TimerFrame;
        Color 
            Colour;

        public Salyut(Texture2D new_t2dTexture, Vector2 new_v2Position, Color newColor) : base(new_t2dTexture, new_v2Position)
        {
            FrameHeight = FrameWidth = 200;
            FrameCurrent = 0;
            TimerFrame = 0;
            Colour = newColor;
        }

        public new void Update(GameTime _GameTime)
        {
            rRect = new Rectangle(FrameCurrent * FrameWidth, 0, FrameWidth, FrameHeight);
            v2PositionOriginal = new Vector2(rRect.Width / 2, rRect.Height / 2);
            TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (TimerFrame >= 500)
            {
                if (FrameCurrent < 2)
                {
                    FrameCurrent++;
                }
                else
                {
                    FrameCurrent = 0;
                }
                TimerFrame = 0;
            }
        }

        public new void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(t2dTexture, v2Position, rRect, Colour, 0f, v2PositionOriginal, 1.0f, SpriteEffects.None, 0.899f);
        }

    }

    class Rain : Objects
    {
        float
            TimerFrame;

        public Rain(Texture2D new_t2dTexture, Vector2 new_v2Position) : base(new_t2dTexture, new_v2Position)
        {
            TimerFrame = 0;
        }

        public new void Update(GameTime _GameTime)
        {
            rRect = new Rectangle((int)v2Position.X, (int)v2Position.Y, t2dTexture.Width, t2dTexture.Height);
            v2PositionOriginal = new Vector2(rRect.Width / 2, rRect.Height / 2);
            TimerFrame += (float)_GameTime.ElapsedGameTime.TotalMilliseconds;
            if (TimerFrame >= 10)
            {
                v2Position.Y += 50;
            }
            if (v2Position.Y > 700)
            {
                v2Position.Y = -100;
            }
        }

        public new void Draw(SpriteBatch _SpriteBatch)
        {
            _SpriteBatch.Draw(t2dTexture, v2Position, rRect, Color.White, 0f, v2PositionOriginal, 1.0f, SpriteEffects.None, 0.899f);
        }

    }
}