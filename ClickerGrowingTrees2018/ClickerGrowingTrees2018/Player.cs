﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickerGrowingTrees2018
{
    class Player
    {
        // private пременные
        Texture2D
            t2dTexture;             // Текстура
        Rectangle
            rRect;                  // Прямоугольник для отрисовки  
        Vector2
            v2Position,             // Вектор позиции
            v2PositionOriginal,     // Вектор оригинальной позиции
            v2Velosity;             // Вектор двжиения
        Color
            colorPlayer,            // Цвет объекта
            colorPlayerSave;
        KeyboardState
            ksKeys;                 // Кнопки клавиатуры
        MouseState
            msMouse;                // Кнопки и курсор мыши
        public Rectangle
            rRectInt;               // Прямоугольник ждя взаимодействия с другими объектами
        float
            fSizePlayer;            // Размер объекта
        bool
            isMousePressed;         // Флаг нажатия кнопки мыши
        // public переменные
        public int
            nClick;                 // Кол-во кликов
        public string
            sName;                  // Имя игрока



        public Player(Texture2D new_t2dTexture, Vector2 new_v2Position)
        {
            t2dTexture = new_t2dTexture;
            v2Position = new_v2Position;
            colorPlayer = new Color(0, 255, 0, 255);
            colorPlayerSave = colorPlayer;
            v2Velosity = new Vector2(5, 5);
            sName = "";
            nClick = 0;
            isMousePressed = false;
            fSizePlayer = 0.2f;
        }

        /// <summary>
        /// Логика объекта
        /// </summary>
        public void Update(GameTime gameTime)
        {
            // Реализация логики объекта
            ksKeys = Keyboard.GetState();
            msMouse = Mouse.GetState();
            if (msMouse.LeftButton == ButtonState.Pressed)
            {
                if (!isMousePressed)
                {
                    nClick++;
                    fSizePlayer += 0.01f;
                }
                isMousePressed = true;
            }
            else
            {
                isMousePressed = false;
            }
            // Определение прямоугольника для отрисовки
            rRect = new Rectangle((int)v2Position.X, (int)v2Position.Y, t2dTexture.Width, t2dTexture.Height);
            // Определение прямоугольника для взаимодействия
            rRectInt = new Rectangle();
            rRectInt.X = (int)(v2Position.X - t2dTexture.Width* fSizePlayer / 2);
            rRectInt.Y = (int)(v2Position.Y - t2dTexture.Height* fSizePlayer + (t2dTexture.Height * fSizePlayer / 3));
            rRectInt.Width = (int)(t2dTexture.Width * fSizePlayer - ((t2dTexture.Width * fSizePlayer / 4)));
            rRectInt.Height = (int)(t2dTexture.Height * fSizePlayer - (t2dTexture.Height * fSizePlayer / 3));
            // Определение центальной точки
            v2PositionOriginal = new Vector2(t2dTexture.Width/2, t2dTexture.Height);
        }


        /// <summary>
        /// Отрисовка объекта
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(t2dTexture, v2Position, new Rectangle(0, 0, t2dTexture.Width, t2dTexture.Height), colorPlayer, 0f, v2PositionOriginal, fSizePlayer, SpriteEffects.None, 0.5f);

        }

        /// <summary>
        /// Определение соприкосновения объектов
        /// </summary>
        /// <param name="new_rRectInt">Класс объекта, соприкосновение которого необходимо определить</param>
        /// <param name="new_isActiveObj">Флаг статуса объекта, определяющий его существование</param>
        /// <returns>True - соприкосновение объектов произошло. False - соприкосновение объектов не произошло</returns>
        public bool Interaction(Rectangle rRectInt, bool isActiveObj)
        {
            if (!isActiveObj)
            {
                return false;
            }
            if (rRectInt.Intersects(rRectInt))
            {
                return true;
            }
            return false;
        }



    }
}
